
# Show Service Level Plugin for MSM

This plugin automatically shows service level information for a request.

## Compatible Versions

| Plugin  | MSM         |
|---------|-------------|
| 1.1.0   | 14.10.5     |
| 1.1.0   | 14.13.0     |
| 1.1.0   | 14.24.0     |
| 1.1.0   | 14.24.1     |
| 1.1.1   | 15.1+       |

## Installation

Please see your MSM documentation for information on how to install plugins.

Once the plugin has been installed the following will happen:

+ This plugin automatically shows service level information.

## Usage

The plugin is automatically loaded when you load a request.

## Contributing

We welcome all feedback including feature requests and bug reports. Please raise these as issues on GitHub. If you would like to contribute to the project please fork the repository and issue a pull request.